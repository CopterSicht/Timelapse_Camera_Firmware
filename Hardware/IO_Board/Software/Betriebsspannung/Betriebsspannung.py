from spidev import SpiDev
from time import sleep

spi = SpiDev()
spi.open(0,0)
    
def readMPC(channel):
        adc = spi.xfer2([1, (8 + channel) << 4, 0])
        data = ((adc[1] & 3) << 8) + adc[2]
        return data
while 1:
    
    value = readMPC(0)
    print("Anliegende Spannung: %.2f" % (((value / 1023.0 * 3.3)*6.62)*0.95) )
    sleep(0.5)
 
 
