# coding: utf-8

import RPi.GPIO as GPIO
from spidev import SpiDev
from time import sleep


spi = SpiDev()
spi.open(0,0)

GPIO.setmode(GPIO.BCM)
GPIO.setup(25, GPIO.OUT)
GPIO.setup(18, GPIO.OUT)

def readMPC(channel):
        adc = spi.xfer2([1, (8 + channel) << 4, 0])
        data = ((adc[1] & 3) << 8) + adc[2]
        return data
    
while 1:
    value = readMPC(1)
    if (value / 1023.0 * 3.3) > 0:
        GPIO.output(25, GPIO.HIGH)
        GPIO.output(18, GPIO.HIGH)
    else:
        GPIO.output(25, GPIO.LOW)
        GPIO.output(18, GPIO.LOW)
    sleep(0.1)
 
