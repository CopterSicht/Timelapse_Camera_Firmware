#! /bin/bash

hostname=schuttercloud.com
username=uploader
password=XXXXXXXXXXXXXXXXXXXXXXXXXX
cameraDir=/camera03/latest/
webDir=test
webFile=TLC03
mask=mask.png


echo starting

ftp -nv <<EOF
open $hostname
user $username $password
cd $cameraDir
nlist *.jpg index.txt
bye
EOF

latest_file=`tail -1 index.txt`
rm index.txt

ftp -nv <<EOF
open $hostname
user $username $password
binary
cd $cameraDir
get $latest_file
bye
EOF

# mask images
composite $mask $latest_file $webFile.jpg
rm $latest_file

# resize images
convert $webFile.jpg -resize 1083x813 ${webFile}_resized.jpg

mv $webFile.jpg $webDir

mv ${webFile}_resized.jpg $webDir
