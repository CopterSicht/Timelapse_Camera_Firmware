from spidev import SpiDev
 
spi = SpiDev()
spi.open(0,0)
    
def read(channel = 0):
        adc = spi.xfer2([1, (8 + channel) << 4, 0])
        data = ((adc[1] & 3) << 8) + adc[2]
        return data

value = read(channel = 0)

print("Anliegende Spannung: %.2f" % ((value / 1023.0 * 3.3)*6.62) )

 
 
