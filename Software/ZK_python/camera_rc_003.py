from picamera import PiCamera
from time import sleep
from ftplib import FTP
from fractions import Fraction
from datetime import datetime
import ftplib
import time
import os
import logging

camera = PiCamera()
ftp = FTP('schuttercloud.com','uploader','1234')
ftp.cwd('/camera01')

camera.resolution = (3280,2464) #aufloesung der bilder maximal 3280,2464
#camera.framerate = 15 #Fraction(2,6) 		#framrate 15 bei video und 2592x1944
camera.shutter_speed = 500000
camera.awb_mode = 'auto'
camera.exposure_mode = 'auto'
wakeup_Time = [7,7,6,5,5,4,4,5,5,6,6,6]
i=0
camera_number = '01'
log_name = camera_number+'_'+str(datetime.now().strftime("%A %d. %B %Y"))+'.log'
log_name = log_name.replace(" ","-")
logging.basicConfig(filename=log_name,format='%(asctime)s [%(levelname)s] %(message)s',level=logging.DEBUG,datefmt='%d/%m/%Y %H:%M:%S')

print ('New ftp connection successful! current dicertory is: '+ftp.pwd())
logging.info('New ftp connection successful! current dicertory is: '+ftp.pwd())
#camera.brightness = 50 	#0 bis 100; default 50
#camera.contrast = 0		#-100 bis 100; default 0
#camera.iso = 0 			#0 bis 1600; default 0(auto)
#camera.zoom = (0, 0, 1.0, 1.0) 		#0.0 bis 1.0 (x, y, w, h)
#camera.sharpness = 0 	#-100 bis 100; default 0

def uploadToFTP():
	print('trying to upload file ...')
	logging.info('trying to upload file ...')
	try:
		ftp.storbinary('STOR ' + picture_name, open(picture_path,'rb'),1024)
		ftp.sendcmd('SITE CHMOD 754 '+picture_name)
		os.remove(picture_path)	#TODO fallback test
		print('upload successful!')
		logging.info('upload successful!')
	except ftplib.all_errors as e:
		print ('upload failed!')
		print (e)
		logging.error('upload failed!')
		logging.error(str(e))
		try:
			print('trying to reconnect ...')
			logging.info('trying to reconnect ...')
			ftp.connect('schuttercloud.com')
			ftp.login('uploader','1234')
			print('re-connecting successful!')
			logging.info('re-connecting successful!')
			ftp.storbinary('STOR ' + picture_name, open(picture_path,'rb'),1024)
			ftp.sendcmd('SITE CHMOD 754 '+picture_name)
			os.remove(picture_path)	#TODO fallback test
			print('upload successful!')
			logging.info('upload successful!')
		except ftplib.all_errors as e9:
			print('re-connecting failed!')
			print(e9)
			logging.error('re-connecting failed!')
			logging.error(str(e9))

	time_sleep_unix = time_next_unix - time.time()
	print("sleeping now",time_sleep_unix,"seconds"+'\n')
	logging.info('sleeping now '+str(time_sleep_unix)+' seconds'+'\n')
	sleep(time_sleep_unix)
	return

while 1:
	
	time_next_unix = time.time() + 300
	time_name = time.asctime(time.localtime(time.time()))
	time_name = time_name.replace(" ","_")
	time_name = time_name.replace(":","-")
	picture_name = '01_'+str(i).zfill(9)+'_'+time_name + '.jpg'
	picture_path = '/home/pi/Pictures/' + picture_name
	
	print ("Zeit Name:", time_name)
	
	try:
		camera.capture(picture_path)	#nimmt bild 'image.jpg' auf
		i=i+1
	except Exception as e2:
		logging.error(str(e2))
	
	print ('Picture size:',os.path.getsize(picture_path),"Byte")
	logging.info('Picture size: '+str(os.path.getsize(picture_path))+' Byte')
	if os.path.getsize(picture_path) < 1800000:
		print ('it seems to be night!')
		logging.info('it seems to be night!')
		if datetime.now().hour > wakeup_Time[datetime.now().month] and datetime.now().hour < 17:
			print ('cannot be night!')
			logging.warning('cannot be night!')
			uploadToFTP()
		else:
			#uploading log file
			print ('skipping picture upload!')
			print('uploading todays log befor going to sleep!')
			logging.info('skipping picture upload!')
			logging.info('uploading todays log befor going to sleep!')
			try:
				ftp.storbinary('STOR ' + log_name, open('/home/pi/'+log_name,'rb'),1024)
				ftp.sendcmd('SITE CHMOD 754 '+log_name)
				print('uploaded log-file successful!')
				logging.info('uploaded log-file successful!')
			except ftplib.all_errors as e:
				print ('upload failed!')
				print (e)
				logging.error('upload failed!')
				logging.error(str(e))
				try:
					print('trying to reconnect ...')
					logging.info('trying to reconnect ...')
					ftp.connect('schuttercloud.com')
					ftp.login('uploader','1234')
					print('re-connecting successful!')
					logging.info('re-connecting successful!')
					ftp.storbinary('STOR ' + log_name, open('/home/pi/'+log_name,'rb'),1024)
					ftp.sendcmd('SITE CHMOD 754 '+log_name)
					print('uploaded log-file successful!')
					logging.info('uploaded log-file successful!')
				except ftplib.all_errors as e8:
					print('re-connecting failed!')
					print(e8)
					logging.error('re-connecting failed!')
					logging.error(str(e8))
			
			#sleep until next morning		
			print ('going to sleep for a while ...')
			logging.info('going to sleep for a while ...')
			midnight = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
			seconds = (datetime.now() - midnight).seconds
			sleep_time = 86400-seconds+(wakeup_Time[datetime.now().month-1]*60*60)
			print ('sleeping now '+str(sleep_time)+' seconds or until '+str(wakeup_Time[datetime.now().month-1])+'am!')
			logging.info('sleeping now '+str(sleep_time)+' seconds or until '+str(wakeup_Time[datetime.now().month-1])+'am!')
			sleep(sleep_time-10)
			
			#maybe needed! needs to be tested
#			ftp.connect('schuttercloud.com')	
#			ftp.login('uploader','1234')
			
			#wake up 10 seconds before we should for exact start
			print(str((datetime.now() - midnight).seconds)+' < '+str((wakeup_Time[datetime.now().month-1]*60*60)))
			while(datetime.now() - midnight).seconds < (wakeup_Time[datetime.now().month-1]*60*60):
				sleep(0.5)
			log_name = camera_number+'_'+str(datetime.now().strftime("%A %d. %B %Y"))+'.log'
			log_name = log_name.replace(" ","-")
			logging.basicConfig(filename=log_name)		
			print('Time to wakeup! \n')
			logging.info('Time to wakeup! \n')
	else:
		uploadToFTP()
